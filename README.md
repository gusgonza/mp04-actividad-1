# Actividad 1 - MP04 -> Phone Details

Este proyecto muestra los detalles de diferentes teléfonos, incluyendo información sobre el teléfono, capacidad, pantalla, cámaras y más.

## Requerimientos Implementados

- **Hover en la tabla**: Se ha añadido un efecto de hover a la tabla.
- **3 tipos de letras de Google Fonts**:
  - Se ha utilizado la fuente **Open Sans** para el encabezado h1 "Phone details".
  - La fuente **Montserrat** se ha aplicado a las cabeceras de las columnas.
  - Se ha utilizado la fuente **Roboto** para el texto general de la tabla.
- **Pseudoclases para colorear líneas impares**: Se ha aplicado un color específico a las líneas impares de la tabla.

## Contenido del Repositorio

- **img/**: Carpeta que contiene las imágenes utilizadas en el proyecto.
- **index.html**: Archivo HTML principal del proyecto.
- **styles.css**: Archivo CSS que contiene los estilos utilizados en el proyecto.

## Captura de Pantalla

![Captura de pantalla de la versión final del proyecto](https://gitlab.com/gusgonza/mp04-actividad-1/-/raw/main/img/finalVersion.png?ref_type=heads)

[![Estado de construcción](https://img.shields.io/static/v1?label=Estado%20de%20Construcción&message=Finalizado&color=success)](https://gitlab.com/gusgonza/mp04-actividad-1/-/tree/main)

